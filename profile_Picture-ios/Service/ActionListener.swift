//
//  ActionListener.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation

 protocol ActionListener {
    func start()
    func success(from:String,data: Any)
    func end()
    func fail()
}
