//
//  FetchStoredData.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation

import Foundation

class FetchStoredData {
    
    public func FetchList() -> [UserWithPosition] {
        if let data = UserDefaults.standard.data(forKey: "ListConversation"),
            let f = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data),
            let f0 = f as? [UserWithPosition] {
            return f0
        }
        return [UserWithPosition]()
    }
}
