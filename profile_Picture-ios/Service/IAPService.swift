//
//  IAPService.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 19.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import StoreKit

class IAPService : NSObject {
    
    static let shared = IAPService()
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    public var Refresh : Refresh?
    
    public func getProducts(){
        let productIDs : Set = [IAPProduct.adds_Free.rawValue]
        
        let request = SKProductsRequest(productIdentifiers: productIDs)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    public func purchase( product : IAPProduct) {
        if SKPaymentQueue.canMakePayments() {
            guard let productToPurchase = products.filter({ $0.productIdentifier == product.rawValue }).first else {return}
            let payment  = SKPayment(product: productToPurchase)
            paymentQueue.add(payment)
        }
        
    }
        
}
extension IAPService : SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        for product in products {
            print(product.localizedTitle)
        }
    }
}

extension IAPService : SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print(transaction.transactionState.Status(), transaction.payment.productIdentifier)
            switch transaction.transactionState {
            case .purchasing : print("purchasing")
            case .restored : print("restored")
            case .purchased :
                print("purchased")
                UserDefaults.standard.set(true, forKey: "isPro")
                Refresh?.refreshPage()
                queue.finishTransaction(transaction)
            default :queue.finishTransaction(transaction)
            }
        }
    }
}

extension SKPaymentTransactionState {
    func Status() -> String {
        switch self {
        case .deferred : return "deferred"
        case .failed: return "failed"
        case .purchased: return  "purchased"
        case .purchasing: return "purchasing"
        case .restored: return "restored"
            
        }
    }
}

