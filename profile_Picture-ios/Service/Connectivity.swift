//
//  Connectivity.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 19.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
