//
//  ServicePaths.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Alamofire

class ServicePaths {
    static let SearchUsers = "/web/search/topsearch/"
    static let UserDetail = "/api/v1/users/"
    static let otherApps = "/android/apps-ios-bigprofilephoto.json"
}
