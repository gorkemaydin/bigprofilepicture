//
//  ToastMessage.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Toaster


class ToastMessage{
    static var shared = ToastMessage()
    private init() {}
    
    func Error(text:String,delay:TimeInterval,duration:TimeInterval){
        configureAppearance(color: .red)
        Toast(text: text, delay: delay, duration: duration).show()
    }
    func Success(text:String,delay:TimeInterval,duration:TimeInterval){
        configureAppearance(color: .green)
        Toast(text: text, delay: delay, duration: duration).show()
    }
    func configureAppearance(color:UIColor) {
        let appearance = ToastView.appearance()
        appearance.backgroundColor = color
        appearance.textColor = .white
        appearance.font = .boldSystemFont(ofSize: 16)
        appearance.textInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        appearance.bottomOffsetPortrait = 100
        appearance.cornerRadius = 20
    }
}
