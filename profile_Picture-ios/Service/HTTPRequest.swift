//
//  HTTPRequest.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

public class HTTPRequest{
    
    public init(){}
    static let shared = HTTPRequest()
    
    func Service (method:String,
                  path:String,
                  additionalPath : String? = nil,
                  parameters:[String:String]? = nil,
                  actionListener:ActionListener){
        
        if !Connectivity.isConnectedToInternet() {
            ToastMessage.shared.Error(text: NSLocalizedString("err_connection_msg", comment: "Error"), delay: 0, duration: 4)
            return
        }
        
        var _url = URLComponents(string: _URL.BaseURL + path)
    
        if let AddPath = additionalPath {
            _url = URLComponents(string: _URL.BaseURL + path  + AddPath)
        }
        
        actionListener.start()

        if parameters != nil{
            var queryItems = [URLQueryItem]()
            for parameter in parameters!{
                let queryItem = URLQueryItem(name: parameter.key, value: parameter.value)
                queryItems.append(queryItem)
            }
            _url?.queryItems = queryItems
        }
        var request = URLRequest(url: (_url?.url)!)
        
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
       
        let pre = Locale.preferredLanguages[0]
        request.setValue(pre, forHTTPHeaderField: "lang")
        

        
        Alamofire.request(request).responseJSON {
            response in
            
            if let code = response.response?.statusCode {
                
                if(code == 200 /*response.result.isSuccess*/){
                    
                    actionListener.success(from: path, data: response.result.value!)
                    
                }else if response.result.isFailure {
                    ToastMessage.shared.Error(text: NSLocalizedString("service_is_continue_warn", comment: "Error"),
                                              delay: 0, duration: 4)
                    actionListener.fail()
                }
            }
        }
    }
    func makeJSONData<T : Codable>(_ value: T) -> Data {
        var jsonData = Data()
        let jsonEncoder = JSONEncoder()
        
        do {
            jsonData = try jsonEncoder.encode(value)
        }
        catch {
        }
        return jsonData
    }
}

