//
//  storeData.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation

class storeData {
    
    public func StoreList(_ value: [UserWithPosition]) {
        if #available(iOS 11.0, *) {
            let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
            UserDefaults.standard.set(encodedData, forKey: "ListConversation")
        } else {
            // Fallback on earlier versions
        }
    }
}
