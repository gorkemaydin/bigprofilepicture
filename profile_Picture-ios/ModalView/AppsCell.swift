
//
//  AppsCell.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit

class AppsCell: UITableViewCell {
    @IBOutlet weak var AppLogo: UIImageView!
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        AppLogo.layer.borderWidth = 1
        AppLogo.layer.masksToBounds = false
        AppLogo.layer.borderColor = UIColor.lightGray.cgColor
        AppLogo.layer.cornerRadius = AppLogo.frame.height/2
        AppLogo.clipsToBounds = true
    }
}
