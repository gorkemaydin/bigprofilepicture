//
//  RadiusView.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit

@IBDesignable
class RadiusView : UIView {
    
    @IBInspectable var radius : CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

