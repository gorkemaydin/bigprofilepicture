//
//  SearchCell.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nickName: UILabel!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var verifyImage: UIImageView!
    @IBOutlet weak var followerNumber: UILabel!
    
    override func awakeFromNib() {
        profilePicture.layer.borderWidth = 1
        profilePicture.layer.masksToBounds = false
        profilePicture.layer.borderColor = UIColor.lightGray.cgColor
        profilePicture.layer.cornerRadius = profilePicture.frame.height/2
        profilePicture.clipsToBounds = true
    }
}
