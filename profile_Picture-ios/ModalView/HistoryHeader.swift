//
//  HistoryHeader.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit

class HistoryHeader: UIView {

    @IBOutlet weak var clearView: RadiusView!
    @IBOutlet weak var HistoryHeader: UILabel!
    @IBOutlet weak var ClearText: UILabel!
    @IBOutlet weak var ClearViewHeight: NSLayoutConstraint!
    @IBOutlet weak var infoImage: RadiusView!
    
    override func awakeFromNib() {
        Localize()
    }
    private func Localize() {
        HistoryHeader.text = NSLocalizedString("history", comment: "history")
    }
}
