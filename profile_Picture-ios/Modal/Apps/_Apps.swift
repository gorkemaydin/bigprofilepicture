//
//  _Apps.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class _Apps :JSONDecodable {
    
    public var id : String?
    public var name : String?
    public var description : String?
    public var logoUrl : String?
    public var price : String?
    public var packageName : String?
    
    required init?(json: JSON) {
        if let Id : String = "id" <~~ json {self.id = Id}
        if let Name : String = "name" <~~ json {self.name = Name}
        if let Description : String = "description" <~~ json {self.description = Description}
        if let LogoUrl : String = "logoUrl" <~~ json {self.logoUrl = LogoUrl}
        if let Price : String = "price" <~~ json {self.price = Price}
        if let PackageName : String = "packageName" <~~ json {self.packageName = PackageName}
    }
}

//"id":"1",
//"name":"Fake Chat (Direct Message)",
//"description":"Create fake chats!",
//"logoUrl":"http://tiawy.com/android/images/fakechat.png",
//"price":"FREE",
//"packageName":"https://itunes.apple.com/us/app/fake-chat-direct-message/id1170817680"
