//
//  OnboardingConfigurations.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class Onboarding_configurations : JSONDecodable {
    public var type: Int?
    public var name : String?
    public var description: String?
    public var verticalID: Int?
    public var valueProps: [ValueProp]?
    
    public required init?(json: JSON) {
        if let Type : Int = "type" <~~ json {self.type = Type}
        if let Name : String = "name" <~~ json {self.name = Name}
        if let Description : String = "description" <~~ json {self.description = Description}
        if let VerticalID : Int = "vertical_id" <~~ json {self.verticalID = VerticalID}
        if let ValueProbs : [ValueProp] = "value_props" <~~ json {self.valueProps = ValueProbs}
    }
}
