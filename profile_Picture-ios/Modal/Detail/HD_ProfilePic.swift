//
//  HD_ProfilePic.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class hd_profile_pic_url_info : JSONDecodable {
   
    public var width : Int?
    public var height: Int?
    public var url: String?
    
    public required init?(json: JSON) {
        if let Width : Int = "width" <~~ json {self.width = Width}
        if let Height : Int = "height" <~~ json {self.height = Height}
        if let URL : String = "url" <~~ json {self.url = URL}
    }

}
