//
//  UserDetail.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class UserDetail : JSONDecodable {
   
    public var pk: Int?
    public var username, fullName: String?
    public var isPrivate: Bool?
    public var profilePicURL: String?
    public var profilePicID: String?
    public var isVerified : Bool?
    public var hasAnonymousProfilePicture: Bool?
    public var mediaCount : Int?
    public var followerCount : Int?
    public var followingCount : Int?
    public var followingTagCount: Int?
    public var biography : String?
    public var externalURL: String?
    public var onboardingConfigurations : [Onboarding_configurations]?
    public var reelAutoArchive: String?
    public var usertagsCount: Int?
    public var isInterestAccount: Bool?
    public var hdProfilePicVersions: [hd_profile_pic_url_info]?
    public var hdProfilePicURLInfo: hd_profile_pic_url_info?
    public var hasHighlightReels : Bool?
    public var isPotentialBusiness : Bool?
    public var autoExpandChaining : Bool?
    public var highlightReshareDisabled: Bool?
    
    public required init?(json: JSON) {
        if let Pk : Int = "pk" <~~ json {self.pk = Pk}
        if let UserName : String = "username" <~~ json {self.username = UserName}
        if let FullName : String = "full_name" <~~ json {self.fullName = FullName}
        if let IsPrivate : Bool = "is_private" <~~ json {self.isPrivate = IsPrivate}
        if let ProfilePictureURL : String = "profile_pic_url" <~~ json {self.profilePicURL = ProfilePictureURL}
        if let ProfilePictureId : String = "profile_pic_id" <~~ json {self.profilePicID = ProfilePictureId}
        if let IsVerified : Bool = "is_verified" <~~ json {self.isVerified = IsVerified }
        if let HasAnn : Bool = "has_anonymous_profile_picture" <~~ json {self.hasAnonymousProfilePicture = HasAnn}
        if let MediaCount : Int = "media_count" <~~ json {self.mediaCount = MediaCount}
        if let FollowerCount : Int = "follower_count" <~~ json {self.followerCount = FollowerCount}
        if let FollowingCount : Int = "following_count" <~~ json {self.followingCount = FollowingCount}
        if let FollowingTagCount : Int = "following_tag_count" <~~ json {self.followingTagCount = FollowingTagCount}
        if let Biography : String = "biography" <~~ json {self.biography = Biography}
        if let ExternalURl : String = "external_url" <~~ json {self.externalURL = ExternalURl}
        if let OnBoardingConfig : [Onboarding_configurations] = "onboarding_configurations" <~~ json {self.onboardingConfigurations = OnBoardingConfig}
        if let AutoArchive : String = "reel_auto_archive" <~~ json {self.reelAutoArchive = AutoArchive}
        if let UserTagCount : Int = "usertags_count" <~~ json {self.usertagsCount = UserTagCount}
        if let IsInterrestAccount : Bool = "is_interest_account" <~~ json {self.isInterestAccount = IsInterrestAccount}
        if let HDProfilePicVersion :  [hd_profile_pic_url_info] = "hd_profile_pic_versions" <~~ json {self.hdProfilePicVersions = HDProfilePicVersion}
        if let HDProfilePicURL : hd_profile_pic_url_info = "hd_profile_pic_url_info" <~~ json {self.hdProfilePicURLInfo = HDProfilePicURL}
        if let HasHighlight : Bool = "has_highlight_reels" <~~ json {self.hasHighlightReels = HasHighlight}
        if let IsPotantiolBuss : Bool = "is_potential_business" <~~ json {self.isPotentialBusiness = IsPotantiolBuss}
        if let AutoExpand : Bool = "auto_expand_chaining" <~~ json {self.autoExpandChaining = AutoExpand}
        if let HighLight : Bool = "highlight_reshare_disabled" <~~ json {self.highlightReshareDisabled = HighLight}
    }
    

}
