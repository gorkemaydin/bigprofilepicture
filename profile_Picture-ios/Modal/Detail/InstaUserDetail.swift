//
//  InstaUserDetail.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class InstaUserDetail : JSONDecodable {
   
    public var user: UserDetail?
    public var status: String?
    
    public required init?(json: JSON) {
        if let User : UserDetail = "user" <~~ json {self.user = User}
        if let Status : String = "status" <~~ json {self.status = Status }
    }
    
}
