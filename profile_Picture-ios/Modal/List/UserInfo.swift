//
//  UserInfo.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class UserInfo : NSObject, JSONDecodable, NSCoding {
    public var pk : String?
    public var username : String?
    public var fullName: String?
    public var isPrivate: Bool?
    public var profilePicURL: String?
    public var profilePicID: String?
    public var isVerified : Bool?
    public var hasAnonymousProfilePicture: Bool?
    public var followerCount: Int?
    public var reelAutoArchive: String?
    public var byline: String?
    public var mutualFollowersCount : Int?
    public var unseenCount: Int?
    

    public required init?(json: JSON) {
        if let Pk : String = "pk" <~~ json {self.pk = Pk}
        if let UserName : String = "username" <~~ json {self.username = UserName}
        if let FullName : String = "full_name" <~~ json {self.fullName = FullName}
        if let isPrivate : Bool = "is_private" <~~ json {self.isPrivate = isPrivate}
        if let ProfilePictureURL : String = "profile_pic_url" <~~ json {self.profilePicURL =  ProfilePictureURL}
        if let ProfilePictureID : String = "profile_pic_id" <~~ json {self.profilePicID = ProfilePictureID}
        if let IsVerified : Bool = "is_verified" <~~ json {self.isVerified = IsVerified}
        if let HasAnonymousProfilePicture : Bool = "has_anonymous_profile_picture" <~~ json {self.hasAnonymousProfilePicture = HasAnonymousProfilePicture }
        if let FollowerCount : Int  = "follower_count" <~~ json {self.followerCount = FollowerCount}
        if let ReelAutoArchive : String = "reel_auto_archive" <~~ json {self.reelAutoArchive = ReelAutoArchive}
        if let Byline : String = "byline" <~~ json {self.byline = Byline}
        if let MutualFollowersCount : Int = "mutual_followers_count" <~~ json {self.mutualFollowersCount = MutualFollowersCount}
        if let UnseenCount : Int = "unseen_count" <~~ json {self.unseenCount = UnseenCount}
    }
    required init?(coder aDecoder: NSCoder) {
        self.pk = aDecoder.decodeObject(forKey: "pk") as? String ?? ""
        self.username = aDecoder.decodeObject(forKey: "username") as? String ?? ""
        self.fullName = aDecoder.decodeObject(forKey: "fullname") as? String ?? ""
        self.isPrivate = aDecoder.decodeObject(forKey: "isPrivate") as? Bool ?? false
        self.profilePicURL = aDecoder.decodeObject(forKey: "profilePicURL") as? String ?? ""
        self.profilePicID = aDecoder.decodeObject(forKey: "profilePicID") as? String ?? ""
        self.isVerified = aDecoder.decodeObject(forKey: "isVerified") as? Bool ?? false
        self.hasAnonymousProfilePicture = aDecoder.decodeObject(forKey: "hasAnonymousProfilePicture") as? Bool ?? false
        self.followerCount = aDecoder.decodeObject(forKey: "followerCount") as? Int ?? 0
        self.reelAutoArchive = aDecoder.decodeObject(forKey: "reelAutoArchive") as? String ?? ""
        self.byline = aDecoder.decodeObject(forKey: "byline") as? String ?? ""
        self.mutualFollowersCount = aDecoder.decodeObject(forKey: "mutualFollowersCount") as? Int ?? 0
        self.unseenCount = aDecoder.decodeObject(forKey: "hasAnonymousProfilePicture") as? Int ?? 0

    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(pk, forKey: "pk")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(isPrivate, forKey: "isPrivate")
        aCoder.encode(profilePicURL, forKey: "profilePicURL")
        aCoder.encode(profilePicID, forKey: "profilePicID")
        aCoder.encode(isVerified, forKey: "isVerified")
        aCoder.encode(hasAnonymousProfilePicture, forKey: "hasAnonymousProfilePicture")
        aCoder.encode(followerCount, forKey: "followerCount")
        aCoder.encode(reelAutoArchive, forKey: "reelAutoArchive")
        aCoder.encode(byline, forKey: "byline")
        aCoder.encode(mutualFollowersCount, forKey: "mutualFollowersCount")
        aCoder.encode(unseenCount, forKey: "unseenCount")
        
    }
}
