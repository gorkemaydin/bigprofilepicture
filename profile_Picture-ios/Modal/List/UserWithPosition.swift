//
//  UserWithPosition.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class UserWithPosition : NSObject, JSONDecodable, NSCoding {
    
    public var position : Int?
    public var user : UserInfo?
    
    public required init?(json: JSON) {
        if let Position : Int = "position" <~~ json {self.position = Position}
        if let User : UserInfo = "user" <~~ json {self.user = User}
    }
    required init?(coder aDecoder: NSCoder) {
        self.position = aDecoder.decodeObject(forKey: "position") as? Int ?? 0
        self.user = aDecoder.decodeObject(forKey: "user") as? UserInfo 
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(position, forKey: "position")
        aCoder.encode(user, forKey: "user")

    }
}
