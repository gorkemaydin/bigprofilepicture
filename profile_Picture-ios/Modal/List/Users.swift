//
//  Users.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class All_Users : JSONDecodable {
    public var users : [UserWithPosition]?
    
    public required init?(json: JSON) {
        if let Users : [UserWithPosition] =  "users" <~~ json {self.users = Users}
    }
}
