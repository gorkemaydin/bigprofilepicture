//
//  HDProfileSceene.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMobileAds

class HDProfileSceene: UIViewController , UIScrollViewDelegate {
    @IBOutlet weak var scrollImg: UIScrollView! {didSet { DTap_Gesture() }}
    
    @IBOutlet weak var goToProfLabel: UILabel! {didSet { goToProfLabel.text = NSLocalizedString("open_web", comment: "open_web")}}
    @IBOutlet weak var saveLabel: UILabel! {didSet {saveLabel.text = NSLocalizedString("save", comment: "save") }}
    @IBOutlet weak var shareLabel: UILabel! {didSet { shareLabel.text = NSLocalizedString("share", comment: "share")}}
    
    
    public var userDetail : UserDetail?
    public var interstitial: GADInterstitial!
    public var isInrestitialEnable = false
    
    @IBOutlet fileprivate weak var shareButton: UIButton! {didSet { shareButton.addTarget(self, action: #selector(shareImageAct), for: .touchUpInside)}}
    @IBOutlet fileprivate weak var saveButton: UIButton! {didSet { saveButton.addTarget(self, action: #selector(SaveToLibrary), for: .touchUpInside)}}
    @IBOutlet fileprivate weak var goToProfileButton: UIButton! {didSet { goToProfileButton.addTarget(self, action: #selector(GoToProfile), for: .touchUpInside)}}
    
    
    @IBOutlet fileprivate weak var savePictureImage: UIImageView!
    
    @objc private func SaveToLibrary() {
        guard let selectedImage = profile.image else {
            print("Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            ToastMessage.shared.Error(text: error.localizedDescription, delay: 0, duration: 4)
        } else {
           ToastMessage.shared.Success(text: NSLocalizedString("saved", comment: "saved"), delay: 0, duration: 4)
        }
    }

    @IBOutlet fileprivate weak var gotoProfileImage: UIImageView!
    @objc private func GoToProfile() {
        guard   let path = userDetail?.username,
                let url = URL(string: _URL.instagramURL + "/" + path )  else {return}
        
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
    }
    @IBOutlet weak var shareImage: UIImageView!

    @objc private func shareImageAct(){
        guard let im = profile.image else {return}
        
        // set up activity view controller
        let imageToShare = [ im ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    private func DTap_Gesture(){
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(ZoomIn_Out(_:)))
        //tapGR.delegate = self
        tapGR.numberOfTapsRequired = 2
        scrollImg.addGestureRecognizer(tapGR)
    }
    @objc private func ZoomIn_Out(_ recognizer: UITapGestureRecognizer) {
        if scrollImg.zoomScale == 1 {
            scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        }else {
            scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.minimumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        }
    }
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = profile.frame.size.height / scale
        zoomRect.size.width  = profile.frame.size.width  / scale
        let newCenter = profile.convert(center, from: scrollImg)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    lazy var Indicator : UIActivityIndicatorView = {
        let _indicator = UIActivityIndicatorView()
        _indicator.center = self.view.center
        _indicator.style = .whiteLarge
        _indicator.color = UIColor.lightGray
        self.view.addSubview(_indicator)
        return _indicator
    }()

    @IBOutlet weak var profile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Config()
        Zoom()
       
    }
    fileprivate func Config() {
        indicatorConfig()
        addConfig()
    }
    private func indicatorConfig() {
        Indicator.startAnimating()
        if let url = userDetail?.hdProfilePicURLInfo?.url {
            self.navigationController?.navigationBar.topItem?.title = userDetail?.fullName
            profile.downloaded(from: url) {
                self.Indicator.stopAnimating()
                self.Indicator.isHidden = true
            }
        }
    }
    private func addConfig() {
        if isInrestitialEnable {
            interstitial.present(fromRootViewController: self)
        }
    }
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    private func Zoom() {
        scrollImg.delegate = self
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 5.0
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.profile
    }
    private func loadSubscriptionOptions() {
    }

}

