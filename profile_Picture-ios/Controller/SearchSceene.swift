//
//  ViewController.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 17.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit
import Gloss
import GoogleMobileAds

class SearchSceene : UITableViewController {
    private var addCounter = 0
    
    fileprivate var UserList = [UserWithPosition]()
    private var isHistoryEnable = true
    private var ClearButtonCounter = 0
    
    public var interstitial: GADInterstitial!
   /* @IBOutlet weak var addFreeOut: UIBarButtonItem! {
        didSet {
            addFreeOut.title = NSLocalizedString("adds_Free", comment: "adds_Free")
        }
    }*/
    @IBOutlet weak var noAddsImage: UIImageView! {
        didSet{
            noAddsImage.isUserInteractionEnabled = true
            let gesture = UITapGestureRecognizer(target: self, action: #selector(addFree))
            noAddsImage.addGestureRecognizer(gesture)
        }
    }
    @objc private func addFree() {
        IAPService.shared.purchase(product: IAPProduct.adds_Free)
    }
    
    
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    @IBAction func OtherApps(_ sender: Any) {
        _URL.BaseURL = _URL.tiawyURL
        HTTPRequest.shared.Service(method: "GET", path: ServicePaths.otherApps, actionListener: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Config()
        FetchData()


    }
    override func viewDidAppear(_ animated: Bool) {
        adds()
    }
    private func adds() {
        if  !UserDefaults.standard.bool(forKey: "isPro") {
            interstitial = GADInterstitial(adUnitID: "ca-app-pub-2266585235859763/6728650644")
            let request = GADRequest()
            interstitial.load(request)
        }
    }

    fileprivate func Config() {
        HistoryConfig()
        URLConfig()
        tableViewConfig()
        SearchConfig()
        SelfConfig()
        IAPConfig()
    }
    private func IAPConfig(){
        no_AddImageConfig()
        IAPService.shared.getProducts()
        IAPService.shared.Refresh = self
    }
    private func no_AddImageConfig(){
        if UserDefaults.standard.bool(forKey: "isPro") {
            noAddsImage.isUserInteractionEnabled = false
            noAddsImage.isHidden = true
        }
    }
    private func HistoryConfig() {
        if FetchStoredData().FetchList().count == 0 {
            isHistoryEnable = false
        }
    }
    private func URLConfig(){
        _URL.BaseURL = _URL.instagramURL
    }
    fileprivate func FetchData() {
        UserList = FetchStoredData().FetchList()
        tableView.reloadData()
    }
    private func SelfConfig() {
        self.navigationController?.navigationBar.topItem?.title = NSLocalizedString("app_name", comment: "app_name")
        searchBar.becomeFirstResponder()
    }
    private func SearchConfig() {
        searchBar.delegate = self
        if let searchField = searchBar.value(forKey: "searchField") as? UITextField {
            searchField.placeholder = NSLocalizedString("search", comment: "search")
            searchField.addTarget(self, action: #selector(searchDidChange(_:)), for: .editingChanged)
        }
    }
    @objc private func searchDidChange(_ textField : UITextField) {
        guard let t = textField.text else {return}
        if t == "" {
            UserList = FetchStoredData().FetchList()
            if UserList.count == 0 { isHistoryEnable = false }
            else {isHistoryEnable = true}
            tableView.reloadData()
            return
        }
        isHistoryEnable = false
        let parameters = ["context":"blended","query":t]
        HTTPRequest.shared.Service(method: "GET", path: ServicePaths.SearchUsers, parameters : parameters, actionListener: self)
    }
    
    private func tableViewConfig() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            if let navigationController =  segue.destination as? UINavigationController {
                if let target = navigationController.topViewController as? HDProfileSceene {
                    if let Sender = sender as? InstaUserDetail {
                        target.userDetail = Sender.user
                        self.view.endEditing(true)
                        if !UserDefaults.standard.bool(forKey: "isPro") {
                            if addCounter % 2 == 1 {
                                if interstitial.isReady {
                                    target.interstitial = interstitial
                                    target.isInrestitialEnable = true
                                }
                            }
                        }
                        addCounter += 1
                    }
                }
            }
        }else
        if segue.identifier == "toOtherApps" {
            if let navigationController = segue.destination as? UINavigationController {
                if let target = navigationController.topViewController as? AppsListSceene {
                    if let DataToSend = sender as? [_Apps] {
                        target.appsList = DataToSend
                    }
                }
            }
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard   let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchCell,
                let user = UserList[indexPath.row].user,
                let followerCount = user.followerCount,
                let isVerify = user.isVerified else {return UITableViewCell()}
        
        cell.nickName.text = user.username
        cell.fullName.text = user.fullName
        cell.followerNumber.text = followerCount.formattedWithSeparator + NSLocalizedString("followers", comment: "followers")
        
        if isVerify { cell.verifyImage.isHidden = false }
        else { cell.verifyImage.isHidden = true }
        
        if let profilePictureURL = UserList[indexPath.row].user?.profilePicURL {  cell.profilePicture.downloaded(from: profilePictureURL){} }
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var saveData = FetchStoredData().FetchList()
        
        if saveData.isEmpty {
            saveData.append(UserList[indexPath.row])
        }
        
        var isIncluded = true
        for user in saveData {
            if UserList.count == 0 { break }
            if !(user.user?.pk == UserList[indexPath.row].user?.pk) {
                isIncluded = false
            }else { isIncluded = true ; break}
        }
        if !isIncluded {
            saveData.append(UserList[indexPath.row])
        }

        
        storeData().StoreList(saveData)
        if let PK = UserList[indexPath.row].user?.pk {
            HTTPRequest.shared.Service(method: "GET",
                                       path: ServicePaths.UserDetail,
                                       additionalPath: PK + "/info/" ,
                                       actionListener: self)
        }
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let View = Bundle.main.loadNibNamed("HistoryHeader", owner: self, options: nil)?.first as? HistoryHeader {
            let clearGesture = UITapGestureRecognizer(target: self, action: #selector(clearAct(_:)))
            View.clearView.tag = 0
            View.clearView.addGestureRecognizer(clearGesture)
            
            let infoGesture = UITapGestureRecognizer(target: self, action: #selector(clearAct(_:)))
            View.infoImage.tag = 1
            View.infoImage.addGestureRecognizer(infoGesture)
            return View
        }
        return UIView()
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isHistoryEnable { return 44 }
        else {return 0 }
        
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if searchBar.text == "" {return true}
        return false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            UserList.remove(at: indexPath.row)
            if UserList.count == 0 {isHistoryEnable = false}
            storeData().StoreList(UserList)
            tableView.reloadData()
        }
    }
    @objc private func clearAct(_ gesture : UIGestureRecognizer) {
        guard let view = gesture.view else {return}
        switch view.tag {
        case 0:
            guard let View = view.superview as? HistoryHeader else {return}
            if ClearButtonCounter == 0 {
                ClearButtonCounter += 1
                UIView.animate(withDuration: 0.5) {
                    View.ClearViewHeight.constant = 70
                    View.ClearText.text = "Temizle"
                }
            }else {
                var f = FetchStoredData().FetchList()
                f.removeAll()
                storeData().StoreList(f)
                ClearButtonCounter = 0
                UserList.removeAll()
                isHistoryEnable = false
                tableView.reloadData()
            }
        case 1:
            let alert = UIAlertController(title: NSLocalizedString("history", comment: "history"),
                                          message: NSLocalizedString("history_notes", comment: "Notes"),
                                          preferredStyle: .alert)
            let ok = UIAlertAction(title: NSLocalizedString("ok", comment: "ok"), style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
}
extension SearchSceene : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SearchSceene : ActionListener {
    func start() {
        print("started")
    }
    
    func success(from: String, data: Any) {
        if from == ServicePaths.SearchUsers {
            if searchBar.text == "" { return }
            guard let JSONData = data as? JSON else {return}
            if let all_Users = All_Users(json: JSONData), let users = all_Users.users {
                UserList = users
                tableView.reloadData()
            }
        }else
        if from == ServicePaths.UserDetail {
            guard let JSONData = data as? JSON else {return}
            if let userDetail = InstaUserDetail(json: JSONData) {
                self.performSegue(withIdentifier: "toDetail", sender: userDetail)
            }
        }else
        if from == ServicePaths.otherApps {
            _URL.BaseURL = _URL.instagramURL
            var Sender = [_Apps]()
            if let Array = data as? [Any] {
                Array.forEach { (apps) in
                    if let JSONApps = apps as? JSON {
                        if let Apps = _Apps(json: JSONApps) {
                            Sender.append(Apps)
                        }
                    }
                }
                self.performSegue(withIdentifier: "toOtherApps", sender: Sender)
            }
        }
    }
    
    func end() {
        print("end")
    }
    
    func fail() {
        print("fail")
    }
}
extension SearchSceene : Refresh {
    func refreshPage() {
        if UserDefaults.standard.bool(forKey: "isPro") {
            self.noAddsImage.isHidden = true
            self.noAddsImage.isUserInteractionEnabled = false
        }
    }
}
