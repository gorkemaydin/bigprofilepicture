//
//  AppsListSceene.swift
//  profile_Picture-ios
//
//  Created by Görkem Aydın on 18.11.2018.
//  Copyright © 2018 Görkem Aydın. All rights reserved.
//

import UIKit


class AppsListSceene: UITableViewController {
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public var appsList = [_Apps]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        Config()
        
    }
    fileprivate func Config() {
        SelfConfig()
    }
    private func SelfConfig() {
        self.navigationController?.navigationBar.topItem?.title = NSLocalizedString("other_apps", comment: "other_apps")
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appsList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AppsCell") as? AppsCell else {return UITableViewCell()}
        cell.selectedBackgroundView = UIView()
        cell.name.text = appsList[indexPath.row].name
        cell.Description.text = appsList[indexPath.row].description
        if let url = appsList[indexPath.row].logoUrl {cell.AppLogo.downloaded(from: url) }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _url = appsList[indexPath.row].packageName ,let url = URL(string: _url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}


